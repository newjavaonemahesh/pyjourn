[![License](http://img.shields.io/:license-apache-blue.svg?style=flat)](https://bitbucket.org/newjavaonemahesh/pyjourn/LICENSE)

# Pyjourn

[![alt-text-1](https://bytebucket.org/newjavaonemahesh/pyjourn/raw/2462f445cd4a1a4fe6f43a6c89d082476b428f9f/python-100x100.png "Python Programming Language")](https://www.python.org/) [![alt-text-2](https://bytebucket.org/newjavaonemahesh/pyjourn/raw/2462f445cd4a1a4fe6f43a6c89d082476b428f9f/django-100x100.jpg "Django Framework")](https://www.djangoproject.com/)

This app similar to limeroad, but not all that cluttered.

License
----

[![License](http://img.shields.io/:license-apache-blue.svg?style=flat)](https://bitbucket.org/newjavaonemahesh/pyjourn/LICENSE)