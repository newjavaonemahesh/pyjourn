from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium import webdriver

from listings.models import Brand, Product

import collections
import time
import unittest


class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(3)

        x1 = 3
        x2 = 2

        first_pname = "shoes"
        brand_name = "adidas"

        first_products = [first_pname+str(i) for i in range(1, x1+1)]
        brand = Brand.objects.create(name=brand_name, slug=brand_name)
        for i in range(0, x1):
            Product.objects.create(name = first_products[i], slug=first_products[i], brand=brand)

        second_pname = "jeans"
        brand_name = "acne"
        second_products = [second_pname+str(i) for i in range(1, x2+1)]
        brand = Brand.objects.create(name=brand_name, slug=brand_name)
        for i in range(0, x2):
            Product.objects.create(name=second_products[i], slug=second_products[i], brand=brand)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_lists(self, row_text, search_type):
        if search_type == 'product':
            ul_elem = self.browser.find_element_by_id('fme_layered_container')
            result_products = Product.objects.filter(name__contains=row_text)
            expected = [p.name for p in result_products]
            lists = ul_elem.find_elements_by_tag_name('li')
            actual = [p.text for p in lists]
        else:
            ul_elem = self.browser.find_element_by_id('fme_layered_container')

            brand_again = Brand.objects.get(name__contains=row_text)
            result_products = brand_again.product_set.all()

            expected = [p.name for p in result_products]
            lists = ul_elem.find_elements_by_tag_name('li')
            actual = [p.text for p in lists]

        self.assertTrue(collections.Counter(expected) == collections.Counter(actual),
                    "Incorrect results")

    def test_can_create_new_products_brands_and_link(self):

        # Edith clicks the add product link from the navigation menu
        self.browser.get(self.live_server_url)
        # time.sleep(3)
        add_product_link = self.browser.find_element_by_link_text('Add Product')
        add_product_link.click()

        # She notices the page title changes to pyjourn - Add Product
        self.assertIn('pyjourn - Add Product', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('pyjourn - Add Product', header_text)

        # She enters a new product with adidas as brand
        inputbox = self.browser.find_element_by_id('product_id')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Add Product'
        )

        # She types "adidas - another new product" into a text box
        inputbox.send_keys('adidas - another new product')

        # she uses the brand dropdown and selects the brand name Adidas
        brand_elem = self.browser.find_element_by_id('selected_brand')
        for option in brand_elem.find_elements_by_tag_name('option'):
            if option.text == 'Adidas':
                option.click() # select() in earlier versions of webdriver
            break

        inputbox.send_keys(Keys.ENTER)
        # time.sleep(10)

        # She goes to the homepage, searches for the new product, and ensures that
        # it is listed
        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('search')
        inputbox.send_keys('adidas - another new product')
        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)
        self.check_for_row_in_lists('adidas - another new product', 'product')

        # she searches for the brand to which she added the new product and finds
        # the product listed under the brand
        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('search')
        inputbox.send_keys('adidas')
        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)
        adidas_listings_url = self.browser.current_url
        self.assertRegex(adidas_listings_url, '/brands/([\w\-]+)')

        ul_elem = self.browser.find_element_by_id('fme_layered_container')

        brand_again = Brand.objects.get(name__contains="adidas")
        result_products = brand_again.product_set.all()
        expected = "adidas - another new product"
        lists = ul_elem.find_elements_by_tag_name('li')
        actuals = [p.text for p in lists]

        self.assertTrue(expected in actuals)

    def test_can_search_products_or_brands_and_allow_filtering(self):

        # Edith has heard about a cool new online clothing store site, Pyjourn.  She goes
        # to check out its homepage
        self.browser.get(self.live_server_url)
        # time.sleep(3)

        # She notices the page title and header mention the store brand name pyjourn
        self.assertIn('pyjourn', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('pyjourn', header_text)

        # She is offered to search for a product or a brand right away
        inputbox = self.browser.find_element_by_id('search')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Search for products or brands...'
        )


        # She types "shoes" into a text box
        inputbox.send_keys('shoes')

        #When she hits enter, the page reloads, and the page lists products
        #as list

        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)

        self.check_for_row_in_lists('shoes', 'product')

        # There is still a text box to search another product or brand.  She
        # enters "jeans".  Edith is very methodical)
        inputbox = self.browser.find_element_by_id('search')
        inputbox.send_keys('jeans')
        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)

        #the page updates as before, only this time a different set of results
        self.check_for_row_in_lists('jeans', 'product')


        #Edith wants to search a brand.  So she types "adidas".
        # When she hits enter, she is taken to a new URL which lists products
        #as list for that brand
        inputbox = self.browser.find_element_by_id('search')
        inputbox.send_keys('adidas')
        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)

        adidas_listings_url = self.browser.current_url
        self.assertRegex(adidas_listings_url, '/brands/([\w\-]+)')
        self.check_for_row_in_lists('adidas', 'brand')

        #She then tries another brand.  She types "acne".
        # hits enter, she is taken to a different URL which lists products
        #as list for that brand.  She knows URL changes with search word

        inputbox = self.browser.find_element_by_id('search')
        inputbox.send_keys('acne')
        inputbox.send_keys(Keys.ENTER)
        # time.sleep(3)

        acne_listings_url = self.browser.current_url
        self.assertRegex(acne_listings_url, '/brands/([\w\-]+)')
        self.assertNotEqual(adidas_listings_url, acne_listings_url)


        # page also features filters to further narrow her search
        #she chose a filter, the page updates with relevant products narrowed by that filter

        self.fail('Finish the test!')

        #Satisfied, she gets back to sleep


