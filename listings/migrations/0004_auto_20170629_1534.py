# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0003_brand'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='name',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='product',
            name='brand',
            field=models.ForeignKey(default=None, to='listings.Brand'),
        ),
    ]
