from django.db import models


class Brand(models.Model):
    name = models.TextField(default='')
    slug = models.SlugField(
        default='',
        unique=True
    )


class Product(models.Model):
    name = models.TextField(default='')
    slug = models.SlugField(
        default='',
        unique=True
    )
    brand = models.ForeignKey(Brand, default=None)



