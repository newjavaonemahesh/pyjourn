from django.core.urlresolvers import resolve
from django.db import IntegrityError, transaction
from django.template.loader import render_to_string
from django.test import TestCase
from django.http import HttpRequest

from listings.models import Brand, Product
from .views import home_page, new_product, search
import collections
import time
import re


class HomePageTest(TestCase):

    @staticmethod
    def remove_csrf(html_code):
        csrf_regex = r'<input[^>]+csrfmiddlewaretoken[^>]+>'
        return re.sub(csrf_regex, '', html_code)

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve("/")
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html_except_csrf(self):
        request = HttpRequest()
        response = home_page(request)

        expected_html = render_to_string('listings/home.html')
        self.assertEqual(
            self.remove_csrf(response.content.decode()),
            self.remove_csrf(expected_html)
        )


class BrandSearchViewTest(TestCase):

    def test_search_product_with_GET_request(self):

        first_pname = "shoes"
        brand_name = "adidas"
        x = 5
        first_products = [first_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=brand_name)
        for i in range(0, x):
            Product.objects.create(name=first_products[i], slug=first_products[i], brand=brand)

        response = self.client.get('/search/', {'q' : first_pname})

        for i in range(0, x):
            self.assertContains(response, first_products[i])

    def test_search_brand_redirects_slug_url_after_GET_request(self):

        first_pname = "shoes"
        brand_name = "adidas"
        x = 5
        first_products = [first_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=brand_name, slug=brand_name)
        for i in range(0, x):
            Product.objects.create(name=first_products[i], slug=first_products[i], brand=brand)

        response = self.client.get('/search/', {'q' : brand_name})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/brands/%s/' %(brand_name))


class ListingsViewTest(TestCase):

    def test_uses_list_template(self):
        first_pname = "shoes"
        brand_name = "adidas"
        x = 5
        first_products = [first_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=brand_name)
        for i in range(0, x):
            Product.objects.create(name=first_products[i], slug=first_products[i], brand=brand)

        response = self.client.get('/brands/%s/' % (brand_name))
        self.assertTemplateUsed(response, 'listings/listings.html')

    def test_displays_products_for_that_brand(self):
        first_pname = "shoes"
        first_brand_name = "adidas"
        x = 5
        first_products = [first_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=first_brand_name)
        for i in range(0, x):
            Product.objects.create(name=first_products[i], slug=first_products[i], brand=brand)

        second_pname = "jeans"
        second_brand_name = "acne"
        x = 5
        second_products = [second_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=second_brand_name, slug=second_brand_name)
        for i in range(0, x):
            Product.objects.create(name=second_products[i], slug=second_products[i], brand=brand)

        response = self.client.get('/brands/%s/' % (first_brand_name))

        for i in range(0, x):
            self.assertContains(response, first_products[i])

        for i in range(0, x):
            self.assertNotContains(response, second_products[i])


class NewProductTest(TestCase):

    @staticmethod
    def remove_csrf(html_code):
        csrf_regex = r'<input[^>]+csrfmiddlewaretoken[^>]+>'
        return re.sub(csrf_regex, '', html_code)

    def test_new_product_url_resolves_to_add_product_page_view(self):
        found = resolve('/products/new')
        self.assertEqual(found.func, new_product)

    def test_new_product_returns_correct_html_except_csrf(self):
        request = HttpRequest()
        response = new_product(request)

        expected_html = render_to_string('listings/add_product.html')
        self.assertEqual(
            self.remove_csrf(response.content.decode()),
            self.remove_csrf(expected_html)
        )

    def test_saving_a_POST_request(self):

        brand_name = "adidas"
        brand = Brand.objects.create(name=brand_name)

        self.client.post(
            '/products/new',
            data={'product': 'adidas - another new product',
                  'brands': 'adidas'}
        )
        new_product_ = Product.objects.first()
        self.assertEqual(new_product_.name, 'adidas - another new product')


class ProductOrBrandModelTest(TestCase):

    def test_retrieving_products_or_brands_after_adding(self):
        first_pname = "shoes"
        brand_name = "adidas"
        x = 5
        first_products = [first_pname+str(i) for i in range(1, x+1)]
        brand = Brand.objects.create(name=brand_name)

        for i in range(0, x):
            first_products_x = Product()
            first_products_x.name = first_products[i]
            first_products_x.brand = brand
            first_products_x.slug = first_products[i]
            first_products_x.save()

        result_products_1 = Product.objects.filter(name__contains=first_pname)
        expected = [first_pname+str(i) for i in range(1, x+1)]
        actual = [p.name for p in result_products_1]
        self.assertTrue(collections.Counter(expected) == collections.Counter(actual),
                        "Incorrect results")

        brand_again = Brand.objects.get(name__contains=brand_name)
        for p in result_products_1:
            self.assertEqual(p.brand, brand)

        second_pname = "jeans"
        brand_name = "acne"
        x = 5
        second_products = [second_pname+str(i) for i in range(1, x+1)]

        brand = Brand.objects.create(name=brand_name, slug=brand_name)
        for i in range(0, x):
            second_products_x = Product()
            second_products_x.name = second_products[i]
            second_products_x.brand = brand
            second_products_x.slug = second_products[i]
            second_products_x.save()

        result_products_2 = Product.objects.filter(name__contains=second_pname)
        expected = [second_pname+str(i) for i in range(1,x+1)]
        actual = [p.name for p in result_products_2]
        self.assertTrue(collections.Counter(expected) == collections.Counter(actual),
                        "Incorrect results")
        brand_again = Brand.objects.get(name__contains=brand_name)
        for p in result_products_2:
            self.assertEqual(p.brand, brand)

    @transaction.atomic
    def test_brand_slug_must_be_unique(self):
        brand_name = "A Brand"
        slug = "a-brand"

        brand = Brand.objects.create(name=brand_name, slug=slug)
        brand_again = Brand.objects.get(name__contains=brand_name)
        self.assertEqual(brand, brand_again)

        brand_name = "This Brand Not Added"
        slug = "a-brand"

        try:
            brand = Brand.objects.create(name=brand_name, slug=slug)
        except IntegrityError as e:
            if not 'UNIQUE constraint' in e.args[0]:
                raise
        except:
            raise

    @transaction.atomic
    def test_product_slug_must_be_unique(self):

        brand_name = "A Brand"
        brand = Brand.objects.create(name=brand_name)

        product_name = "A Product"
        slug = "a-product"

        product = Product.objects.create(name=product_name, brand=brand, slug=slug)
        product_again = Product.objects.get(name__contains=product_name)
        self.assertEqual(product, product_again)

        product_name = "This Product Not Added"
        slug = "a-product"

        try:
            product = Product.objects.create(name=product_name, brand=brand, slug=slug)
        except IntegrityError as e:
            if not 'UNIQUE constraint' in e.args[0]:
                raise
        except:
            raise
