from django.shortcuts import redirect, render
from django.http import HttpResponse
from listings.models import Brand, Product

import time


def home_page(request):
    result_products_1 = Product.objects.all()
    rows = [p.name for p in result_products_1]
    available_products = rows
    return render(request, 'listings/home.html', {'products': available_products})


def new_product(request):
    if request.method == 'POST':
        product_name = request.POST['product']
        brand_name = request.POST['brands']
        brand = Brand.objects.filter(name__iexact=brand_name).first()
        Product.objects.create(name=product_name, brand=brand)
    return render(request, 'listings/add_product.html')


def search(request):

    try:
        brand = Brand.objects.get(name__contains=request.GET['q'])
    except Brand.DoesNotExist:
        result_products_1 = Product.objects.filter(name__contains=request.GET['q'])
        rows = [p.name for p in result_products_1]
        available_products = rows
        return render(request, 'listings/listings.html', {'products': available_products})

    return redirect('/brands/%s/' %(request.GET['q']))


def view_listings(request, brand_name):

    brand = Brand.objects.get(name__contains=brand_name)
    result_products = brand.product_set.all()
    rows = [p.name for p in result_products]
    available_products = rows
    return render(request, 'listings/listings.html', {'products': available_products})
