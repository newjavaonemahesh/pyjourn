from django.conf.urls import include, url
from listings import views

urlpatterns = [
    # Examples:
    url(r'^$', views.home_page, name='home'),
    url(r'^search/$', views.search, name='search'),
    url(r'^brands/(.+)/$', views.view_listings, name='view_listings'),
    url(r'^products/new$', views.new_product, name='new_product'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
]
